package com.example.testproject.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.testproject.R
import com.example.testproject.databinding.FragmentMenuBinding
import com.example.testproject.viewmodel.viewModelKot

class MenuFragment : Fragment() {
    lateinit var binding: FragmentMenuBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View{
        binding = FragmentMenuBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModelKot = ViewModelProvider(requireActivity())[viewModelKot::class.java]

        binding.button.setOnClickListener {
            viewModelKot.shuffleQuestions()

            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, GameFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }

    }}