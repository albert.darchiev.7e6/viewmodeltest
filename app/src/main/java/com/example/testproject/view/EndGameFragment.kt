package com.example.testproject.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.testproject.R
import com.example.testproject.databinding.FragmentEndGameBinding
import com.example.testproject.databinding.FragmentGameBinding
import com.example.testproject.model.Question
import com.example.testproject.viewmodel.viewModelKot

class EndGameFragment : Fragment() {
    lateinit var binding: FragmentEndGameBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentEndGameBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModelKot = ViewModelProvider(requireActivity())[viewModelKot::class.java]


        binding.totalScore.text = viewModelKot.score.toString()

        viewModelKot.index = 0
        viewModelKot.score = 0
        binding.goBackButton.setOnClickListener {
             parentFragmentManager.beginTransaction().apply {
                    replace(R.id.fragmentContainerView, MenuFragment())
                    setReorderingAllowed(true)
                    addToBackStack("name")
                    commit()
            }
        }
    }

}