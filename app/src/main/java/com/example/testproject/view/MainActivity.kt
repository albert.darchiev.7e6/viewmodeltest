package com.example.testproject.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.testproject.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, LaunchFragment())
            setReorderingAllowed(true)
            addToBackStack("name")
            commit()
        }
    }
}