package com.example.testproject.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils.replace
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.testproject.R
import com.example.testproject.databinding.FragmentLaunchBinding
import com.example.testproject.databinding.FragmentMenuBinding

class LaunchFragment : Fragment() {
    lateinit var binding: FragmentLaunchBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLaunchBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Handler(Looper.getMainLooper()).postDelayed({
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, MenuFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        },1500.toLong())
    }
}

/*
     fun delay(miliSec: Int){
        Handler(Looper.getMainLooper()).postDelayed({
            //CODIGO CON DELAY
        }, miliSec.toLong())
 */

