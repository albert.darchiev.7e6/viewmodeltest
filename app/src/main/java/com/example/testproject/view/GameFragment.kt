package com.example.testproject.view

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.ViewModelProvider
import com.example.testproject.R
import com.example.testproject.databinding.FragmentGameBinding
import com.example.testproject.model.Question
import com.example.testproject.viewmodel.viewModelKot

class GameFragment : Fragment(), View.OnClickListener {

    lateinit var binding: FragmentGameBinding
    lateinit var questionViewModel: viewModelKot //viewmodel
    lateinit var question: Question //dataClass

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentGameBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        questionViewModel = ViewModelProvider(requireActivity())[viewModelKot::class.java]
        questionViewModel.currentQuestion.observe(viewLifecycleOwner) {
            question = it

            binding.statementTv.text = question.word
            binding.answer1Button.text = question.ans2
            binding.answer2Button.text = question.ans1
        }
        binding.answer1Button.setOnClickListener(this)
        binding.answer2Button.setOnClickListener(this)
    }


    @SuppressLint("SuspiciousIndentation")
    override fun onClick(p0: View?) {
    val button = p0 as Button
        if (button.text == question.ans2) questionViewModel.score += 10
        binding.scoreTv.text = questionViewModel.score.toString()

        if (questionViewModel.index != 3) questionViewModel.nextQuestion()
        else {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, EndGameFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
    }

}