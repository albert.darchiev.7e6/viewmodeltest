package com.example.testproject.model

data class Question(val word: String, val ans1: String, val ans2: String)
