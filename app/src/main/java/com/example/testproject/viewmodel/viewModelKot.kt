package com.example.testproject.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.testproject.model.Question
import com.example.testproject.model.QuestionsList

class viewModelKot: ViewModel() {
    var currentQuestion =MutableLiveData<Question>()
    private val questions = QuestionsList.questions

    var index = 0
    var score = 0

    init {
        currentQuestion.postValue(questions[0])
        // currentQuestion = questions[0]
    }

    fun nextQuestion(){
        index++
        currentQuestion.postValue(questions[index])
        // actualQuestion = questions[index]
    }

    fun shuffleQuestions() {
        questions.shuffle()
    }

}